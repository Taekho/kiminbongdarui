package main;

// 등수에 따라 메달의 색 출력
// 1등이면 금, 2등 은, 3등 동, 이외 없음

// case구문 써서 변수로 rank 집어넣고 분류 1,2,3등만 구문에 집어넣고 이외는 default처리
public class Quiz3 {

	public static void main(String[] args) {
		int rank = 1;
		
		switch(rank) {
		case 1:
			System.out.println("메달의 색은 금입니다.");
			break;
		case 2:
			System.out.println("메달의 색은 은입니다.");
			break;
		case 3:
			System.out.println("메달의 색은 동입니다.");
			break;
		default:
			System.out.println("메달이 없습니다.");
		}

	}

}
