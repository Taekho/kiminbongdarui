package main;
// 이거 전 문제랑 비슷한데 get, set, tostring 잘 쓰면 됨 이거 할 때 주소값나와서 귀찮아서 던진거로 기억함
public class Quiz8 {

	public static void main(String[] args) {
		Subject korean = new Subject("국어", 95);
		Subject math = new Subject("수학", 100);
		
		Student student1 = new Student(1111, "남택호", korean, math);
		student1.showStudentInfo();
		System.out.println(student1.toString());
	}

}

class Subject {
	String subjectName;
	int subjectScore;

	public Subject(String subjectName, int subjectScore) {
		this.subjectName = subjectName;
		this.subjectScore = subjectScore;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getSubjectScore() {
		return subjectScore;
	}

	public void setSubjectScore(int subjectScore) {
		this.subjectScore = subjectScore;
	}
	
	

}

class Student {
	int studentID;
	String studentName;
	Subject korean;
	Subject math;

	public Student(int studentID, String studentName, Subject korean, Subject math) {
		this.studentID = studentID;
		this.studentName = studentName;
		this.korean = korean;
		this.math = math;
	}
	
	public Subject getKorean() {
		return korean;
	}

	public void setKorean(Subject korean) {
		this.korean = korean;
	}

	public Subject getMath() {
		return math;
	}

	public void setMath(Subject math) {
		this.math = math;
	}

	public void showStudentInfo() {
		System.out.println("학번 : " + studentID + ", 학생이름 : " + studentName + ", " + korean + "점, " + math + "점 입니다.");
	}

	@Override
	public String toString() {
		return "Student [studentID=" + studentID + ", studentName=" + studentName + ", korean=" + korean + ", math="
				+ math + "]";
	}
	
	

}
