package main;

// 1부터 100까지의 자연수의 합을 구하는데 합이 300을 초과 했을때 마지막으로 더한 숫자와 합을 구해라. 이건 뭐 while구문 돌리면 되는거고
public class Quiz5 {

	public static void main(String[] args) {
		int i = 1;
		int sum = 0;
		
		while (i<=100) {
			sum = sum + i;
			if(sum>300) {
				break;
			}
			i++;
		}
		System.out.println("마지막으로 더하는 수는 " + i + ", 합은 " + sum + "입니다.");

	}

}
