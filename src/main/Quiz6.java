package main;

// 두개의 매개변수를 받아 두 수를 나누는 함수를 만들고, 두 번째 매개변수가 0일 경우 -999를 출력하는 함수를 만들어라 봉달아. main함수 밖에 함수 만들어서 부르면 됨. 변수형태(int) 선언하는거 잊지말고
public class Quiz6 {
	
	public static int divide(int n1, int n2) {
		int result = n1 / n2;
		if(n2 == 0) {
			return -999;
		}
		return result;
	}

	public static void main(String[] args) {
		
		int result = divide(10, 5);
		System.out.println(result);
		
		int result2 = divide(10, 0);
		System.out.println(result2);

	}

}
