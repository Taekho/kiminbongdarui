package main;

// 주어진 수학점수와 영어점수의 총점과 평균점수를 구하는 거였을듯.

public class Quiz1 {

	public static void main(String[] args) {
		int mathScore = 93;
		int engScore = 70;
		
		int totalScore = mathScore + engScore;
		double evgScore = (mathScore + engScore) / 2.0;
		
		System.out.println("수학점수와 영어점수의 총점은 " + totalScore + "입니다.");
		System.out.println("수학점수와 영어점수의 평균점수는 " + evgScore + "입니다.");

	}

}
