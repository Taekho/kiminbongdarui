package main;

// book 이라는 클래스 만들어서 주어진 값 넣고 출력.
public class Quiz7 {

	public static void main(String[] args) {
		
		Book book1 = new Book();
		book1.title = "스프링";
		book1.price = 20000;
		book1.publisher = "한빛출판사";
		book1.numberOfPage = 300;
		
		Book book2 = new Book("자바", 15000, "금빛출판사", 200);
		
		System.out.println(book1);
		System.out.println(book2);
	}

}

class Book {
	String title;
	int price;
	String publisher;
	int numberOfPage;

	public Book() {
		
	}
	
	public Book(String title, int price, String publisher, int numberOfPage) {
		this.title = title;
		this.price = price;
		this.publisher = publisher;
		this.numberOfPage = numberOfPage;
	}

	

}
