package main;

// 점수가 90점 이상이면 '수' 80점 이상이면 '우' 70점 이상이면 '미' 70점 미만이면 '양'을 출력해라. if else if else 써서 구하는데 OO점 이상인 범위를 90<result<99 이런식으로 구하진 않을거라 믿는다. 그냥 10으로 나누고 int에 저장하면 소수점 알아서 버려짐.
public class Quiz2 {

	public static void main(String[] args) {
		int score = 50;
		int result = score / 10;

		if (result == 9) {
			System.out.println('수');
		} else if (result == 8) {
			System.out.println('우');
		} else if (result == 7) {
			System.out.println('미');
		} else {
			System.out.println('양');
		}

	}

}
