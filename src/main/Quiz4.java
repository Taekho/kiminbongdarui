package main;

// 1부터 100까지의 자연수중 5의 배수만 출력하라신다. x배수 출력하는 문제는 숫자를 x로 나눴을 때 나머지 0

public class Quiz4 {

	public static void main(String[] args) {

		for (int i = 1; i <= 100; i++) {
			if (i % 5 == 0) {
				System.out.println(i);
			}
		}

	}

}
